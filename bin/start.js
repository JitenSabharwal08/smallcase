const Application = require('./application')
const Logger = require('../helper/logger')
const logger = new Logger()

// Initializing Application
const app = new Application('adventurehit-api', 'application')

app.on('info', logger.info) // attaching info event handler
app.on('log', logger.log) // attaching log event handler
app.on('error', logger.error) // attaching error event handler
app.on('debug', logger.debug) // attaching debug event handler
app.on('warning', logger.warning) // attaching warning event handler

/**
 * Application Start
 */
app.loadConfig()
// Initializong all the dependencies
  .then(app.initializeDependencies.bind(app))
// Starting the application
  .then(app.start.bind(app))
  .then(() => {
    logger.info('app started')
  })
  .catch(logger.error)

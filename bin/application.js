const http = require('http')
const EventEmitter = require('events')

const startApp = require('./app')
const config = require('../config/config')
const {connectToMongoDb} = require('../lib/dataLayer')

class Application extends EventEmitter {
  constructor () {
    super()
    this.log = this.emit.bind(this, 'log')
    this.warn = this.emit.bind(this, 'warning')
    this.info = this.emit.bind(this, 'info')
    this.error = this.emit.bind(this, 'error')
    this.debug = this.emit.bind(this, 'debug')
  }

  /**
   * initializing with depentant modules
   * a series of dependency verification logic will be executed.
   * 1. verifies db server availablity
 */
  async initializeDependencies () {
    this.log('dependency checks started')
    return connectToMongoDb.call(this)
  }

  /**
   * start the application
   */
  async start () {
    return new Promise((resolve, reject) => {
      const port = normalizePort(process.env.PORT || '3000')
      // Starting Application
      const app = startApp.call(this)
      app.set('port', port)
      // Creating the server
      const server = http.createServer(app)
      /**
        * Listen on provided port, on all network interfaces.
      */
      server.listen(port)
      this.server = {
        'addr': server.address(),
      }
      server.on('error', onError.bind(this))
      server.on('listening', onListening.bind(this))
    })
  }

  async loadConfig () {
    this['db-url'] = config['db-url']
  }
}

module.exports = Application

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort (val) {
  const port = parseInt(val, 10)

  if (isNaN(port)) {
    // named pipe
    return val
  }

  if (port >= 0) {
    // port number
    return port
  }

  return false
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError (error) {
  if (error.syscall !== 'listen') {
    throw error
  }
  const {addr} = this.server
  const bind = typeof port === 'string'
    ? 'Pipe ' + addr.port
    : 'Port ' + addr.port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      this.error(bind + ' requires elevated privileges')
      process.exit(1)
    case 'EADDRINUSE':
      this.error(bind + ' is already in use')
      process.exit(1)
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening (server) {
  const {addr} = this.server
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port
  this.log('Listening on ' + bind)
}

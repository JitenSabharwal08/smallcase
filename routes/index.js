const express = require('express')
const router = express.Router()

/**
 * Get Request: To get all the trades for
 * a given user and return it as Portfolio
 */

router.get('/', (req, res, next) => {
  res.render('index', {title: 'API Documentation'})
})
module.exports = router

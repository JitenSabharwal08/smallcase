const express = require('express')
const router = express.Router()
const {addTrade, getUserTrades, updateTrade, removeTrade, removeAll} = require('../src/apiHandlers/portfolios')
const {parse, generateHoldings, generatePorfolio, generateReturns} = require('../helper/util')
/**
 * Get Request: To get all the trades for
 * a given user and return it as Portfolio
 */
router.get('/', (req, res, next) => {
  const {query} = req
  getUserTrades.call(global.app, query)
    .then(result => {
      result.data = generatePorfolio(result.data)
      res.send(parse(result))
    })
    .catch(e => {
      const result = {}
      result.error = e.toString()
      res.send(parse(result))
    })
})

/**
 * Get Request: To get all the trades for
 * a given user, calculate the holdings and return holdings
 */
router.get('/holdings', (req, res, next) => {
  const {query} = req
  getUserTrades.call(global.app, query)
    .then(result => {
      result.data = generateHoldings(result.data)
      res.send(parse(result))
    })
    .catch(e => {
      const result = {}
      result.error = e.toString()
      res.send(parse(result))
    })
})

/**
 * Get Request: To get all the trades for
 * a given user, calculate the returns and return Cumulative return
 */
router.get('/returns', (req, res, next) => {
  const {query} = req
  getUserTrades.call(global.app, query)
    .then(result => {
      result.data = generateReturns(result.data)
      res.send(parse(result))
    })
    .catch(e => {
      const result = {}
      result.error = e.toString()
      res.send(parse(result))
    })
})

/**
 * POST Request: To add a new trade for a given user
 */
router.post('/addTrade', (req, res, next) => {
  const {body} = req
  return addTrade.call(global.app, body)
    .then(result => {
      res.send(parse(result))
    })
    .catch(e => {
      const result = {}
      result.error = e.toString()
      res.send(parse(result))
    })
})

/**
 * POST Request: To update a trade for a given user
 */
router.post('/updateTrade', (req, res, next) => {
  const {body} = req
  updateTrade.call(global.app, body)
    .then(resp => {
      res.send(parse(resp))
    })
    .catch(e => {
      const result = {}
      result.error = e.toString()
      res.send(parse(result))
    })
})

/**
 * POST Request: To remove a trade for a given user
 */
router.post('/removeTrade', (req, res, next) => {
  const {body} = req
  removeTrade.call(global.app, body)
    .then(resp => {
      res.send(parse(resp))
    })
    .catch(e => {
      const result = {}
      result.error = e.toString()
      res.send(parse(result))
    })
})

/**
 * POST Request: To remove a trade for a given user
 */
router.post('/removeAllTrades', (req, res, next) => {
  const {body} = req
  removeAll.call(global.app, body)
    .then(resp => {
      res.send(parse(resp))
    })
    .catch(e => {
      const result = {}
      result.error = e.toString()
      res.send(parse(result))
    })
})
module.exports = router

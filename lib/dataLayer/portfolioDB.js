const mongodb = require('mongodb')
const { ObjectId } = mongodb
const COLLECTION = 'portfolio'
const {getDb} = require('./connection')
const {generateTrade} = require('../../helper/util')

module.exports = {
  findAllTrade: async (clientId) => {
    const query = {clientId}
    const db = await getDb()
    return db.collection(COLLECTION).find(query, { _id: 0 }).toArray()
  },

  findTrade: async (query, projection = {}) => {
    if (!query) return new Error('Query Required')
    if (query._id) query._id = ObjectId(query._id)
    if (query.clientId) query.clientId = ObjectId(query.clientId)
    const db = await getDb()
    return db.collection(COLLECTION).find(query, projection).toArray()
  },

  insertTrade: async (trade, clientId) => {
    if (!trade) return new Error('Trade Required')
    const tradeId = generateTrade(trade)
    const query = {clientId, stock: trade.stock, tradeId}
    const update = {
      $set: {
        ...trade,
      },
    }
    const db = await getDb()
    return db.collection(COLLECTION).update(query, update, { safe: false, upsert: true })
  },

  updateTrade: async (oldTrade, newTrade, clientId) => {
    const otradeId = generateTrade(oldTrade)
    const ntradeId = generateTrade(newTrade)
    const query = {
      clientId,
      stock: oldTrade.stock,
      tradeId: otradeId,
    }
    const updateSet = {
      $set: {
        tradeId: ntradeId,
        ...newTrade,
      },
    }
    const db = await getDb()
    return db.collection(COLLECTION).update(query, updateSet, { safe: false, upsert: true })
  },

  // eslint-disable-next-line sort-keys
  removeAllTrade: async (clientId) => {
    const query = {
      clientId,
    }
    const db = await getDb()
    return db.collection(COLLECTION).removeMany(query)
  },

  // eslint-disable-next-line sort-keys
  removeTrade: async (trade, clientId) => {
    const tradeId = generateTrade(trade)
    const query = {
      clientId,
      stock: trade.stock,
      tradeId,
    }
    const db = await getDb()
    return db.collection(COLLECTION).remove(query)
  },

}

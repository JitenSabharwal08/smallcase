const portfolioDB = require('./portfolioDB')
const connection = require('./connection')

module.exports = {
  ...portfolioDB,
  ...connection,
}

const {MongoClient} = require('mongodb')
const config = require('../../config/config.json')
const DBURL = config['db-url']
let _db

function connectToMongoDb (callback) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(DBURL, (err, db) => {
      if (err) {
        return reject(err)
      }
      _db = db.db('portfolios')
      resolve(_db)
    })
  })
}
module.exports = {
  connectToMongoDb,
  getDb () {
    if (!_db) return connectToMongoDb()
    return _db
  },
}

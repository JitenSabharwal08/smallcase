# smallcase

## Installation

Use the npm package manager to install all the dependencies

```bash
npm install
```
## Folder Structure

##### bin
-- app.js (Contains the code for express api)
-- start.js (Start and setup the application)
-- application.js (Class that is the application)
##### config
-- config.json (Hold the system configuration such as mongodb url)
##### helper
-- logger.js (Helper logging library for different log types)
-- util.js (Helper Library consisting of resuable functions)
##### lib
-- data-layer
    ---- connection.js (File contains the connection functions)
    ---- portfolioDB.js (File consist of functionality that requuired by the portfolio api)
    ---- index.js (File that assembles all the data layer functions in one file)
    routes
    -- portfolio.js (File consist of routes defined for the portfolio api)
 ##### src
###### apiHandler
----portfolio.js (File helps in handling portfolio routes functionality)
    
##### .eslintrc.js
(File of eslint configuration)
    
##### package.json
(File holds all the Application dependencies, scripts and other details)
    
/**
 * Parse function to prepare output json
 * @param {object} obj {data: <object>, error:<boolean>, errorMsg: <String>}
 */
const parse = obj => {
  const result = {
    success: true,
  }
  if (obj.error) {
    result.success = false
    result.error = obj.error
  }
  result.data = obj.data
  return result
}

/**
 * Function return True/ False depending on the presence of unudefined
 * If a value is falsy but not false then return false
 * else
 * returns true
 * @param {object} obj
 */
const validatePresence = obj => {
  if (!obj) return false
  const keys = Object.keys(obj)
  let result = true
  for (const k of keys) {
    if (!obj[k] && obj[k] !== false) {
      result = false
    }
  }
  return result
}

/**
 * Returns a trade string depending on the values passed
 * @param {object} trade
 */
const generateTrade = trade => {
  // 'BUY 100@900 10/04/2015'
  return `${trade.type} ${trade.quantity}@${trade.price} ${trade.date}`
}

/**
 * Function takes an array of trades and then creates a portfolio
 * @param {array} trades
 */
const generatePorfolio = (trades = []) => trades.reduce((acc, val) => {
  const key = val.stock.toUpperCase()
  if (acc[key]) {
    acc[key].push(val)
  } else {
    acc[key] = [val]
  }
  return acc
}, {})

/**
 * Function takes an array of trades and return holdings
 * @param {array} trades
 */
const generateHoldings = (trades = []) => trades.reduce((acc, val) => {
  const key = val.stock.toUpperCase()
  if (acc[key]) {
    if (val.type.toUpperCase() === 'BUY') {
      acc[key].price = (acc[key].price + val.price) / 2
      acc[key].quantity = (acc[key].quantity || 0) + val.quantity
    } else {
      acc[key].quantity = (acc[key].quantity || 0) - val.quantity
    }
  } else {
    acc[key] = {
      price: val.price,
      quantity: val.quantity,
    }
  }
  return acc
}, {})

/**
 * Function takes an array of trades and return Cjumulative returns
 * @param {array} trades
 */
const generateReturns = (trades = []) => {
  const finalPrice = 100
  const holdings = generateHoldings(trades)
  const keys = Object.keys(holdings)
  const returns = {}
  for (const k of keys) {
    const initialPrice = holdings[k].price
    returns[k] = (finalPrice / initialPrice - 1)
  }
  return returns
}

module.exports = {
  generateHoldings,
  generatePorfolio,
  generateReturns,
  generateTrade,
  parse,
  validatePresence,
}

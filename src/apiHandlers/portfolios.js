const db = require('../../lib/dataLayer')
const {validatePresence} = require('../../helper/util')
/**
 * Default trade object
 */
const defaultTrade = {
  date: undefined,
  price: undefined,
  quantity: undefined,
  stock: undefined,
  type: undefined,
}
/**
 * Function returns all the trades for a given ClientId
 * @param {object} params
 */
const getUserTrades = async function (params = {}) {
  const {clientId = 'guest'} = params
  const result = {}
  try {
    const trades = await db.findAllTrade(clientId)
    result.data = trades
  } catch (e) {
    result.error = e
  }
  return result
}

/**
 * Function adds the given trade to database
 * @param {object} params
 */
const addTrade = async function (params) {
  const { date, quantity, price, type, stock, clientId = 'guest' } = params
  const result = {}
  const trade = {
    date, price, quantity, stock, type,
  }
  if (!validatePresence(trade)) {
    result.error = 'All data required- {date, price, stock, quantity, type(buy/sell)}'
  } else {
    try {
      await db.insertTrade.call(this, trade, clientId)
      result.data = {clientId, trade}
    } catch (e) {
      result.error = e
    }
  }
  return result
}
/**
 * Function updates the given trade with the given update value
 * @param {object} params {trade*: {}, update*: {}}
 */
const updateTrade = async function (params) {
  const { trade, update, clientId = 'guest' } = params
  const result = {}
  const oldTrade = Object.assign(defaultTrade, trade)
  const newTrade = Object.assign({}, trade, update)
  if (!validatePresence(oldTrade) || !validatePresence(newTrade)) {
    result.error = 'Require proper format of input {trade: {date, price, stock, quantity, type(buy/sell)}, update: {type: Sell}'
  } else {
    try {
      await db.updateTrade.call(this, oldTrade, newTrade, clientId)
      result.data = {clientId, updatedTo: {newTrade}}
    } catch (e) {
      result.error = e
    }
  }
  return result
}
/**
 * Function removes the given trade from the database
 * @param {object} params {date, quantity, price, type, stock, clientId}
 */
const removeTrade = async function (params) {
  const {date, quantity, price, type, stock, clientId = 'guest'} = params
  const trade = { date, price, quantity, stock, type }
  const result = {}
  if (!validatePresence(trade)) {
    result.error = 'All data required- {date, price, stock, quantity, type(buy/sell)}'
  } else {
    try {
      await db.removeTrade(trade, clientId)
      result.data = {
        clientId,
        removed: {trade},
      }
    } catch (e) {
      result.error = e
    }
  }
  return result
}

/**
 * Function to clear all the trades for given Client
 * @param {object} params
 */
const removeAll = async function (params) {
  const {clientId = 'guest'} = params
  await db.removeAllTrade(clientId)
}

module.exports = {
  addTrade,
  getUserTrades,
  removeAll,
  removeTrade,
  updateTrade,
}
